# Fibonacci

Main files to use:
- fibonacci_func.mojo (module)
- main.mojo (runable)
- plots.py (runable)

## Speed in Mojo
`main.mojo` is the runable mojo file, which calls the different fibonacci-functions, declared in `fibonacci_func.mojo`. `fibonacci_func.mojo` is a module, containing functions for calculating the n'th fib number in a recursive, itarative and 'optimized' algorithm, and is not meant to be run or built. 
Please only run `main.mojo` via `mojo main.mojo`. This will calculate the 35th fib-number and give the avarage execution time (from 10 runs).

To compare the execution speed, we run the recursive algorithm additionally in Python, C, C++ and Haskell. We saved the results of the avarage execution times from all runs manually in an array in `plots.py`. If you run `plots.py`, you will see our results.

## Code in other languages
On your machine the execution times may vary, so feel free to check the execution times of the recursive algorith in the other languages we provide codes for:

All Python files are analogous to the mojo files: `fibonacci_func.py` contains the functions, `function_time.py` some functions to measure the time and `main.py` executes all fibonacci functions for the 35th number and calls the functions each 10 times, to calculate the avarage execution time.
Please run `main.py` to test.

`fibonacci_func.c`, `fibonacci_func.cpp` and `FibonacciFunc.hs` contain only the recursive fibonacci-function. `fibonacci_func.c` and `fibonacci_func.cpp` can be compiled and run directly and will calculate the 35th fib number and the avarage execution time from 10 runs. To run the Haskell code, compile `Main.hs`.

Compile `fibonacci_func.c`:
> gcc -o fibonacci_func_c fibonacci_func.c

Run with
> ./fibonacci_func_c

Compile `fibonacci_func.cpp`:
> g++ -o fibonacci_func_cpp fibonacci_func.cpp

Run with:
> ./fibonacci_func_cpp

Compile `Main.hs`:
> ghc -o Main Main.hs

Run with
> ./Main