import fibonacci_func as fb
import function_time as ft
#import plots 

def main():
    #Data
    nth_fib_num = 35 # you have to use same value for mojo as well
    iteration_num = 10 # this is to take average execution time, you can change it as you wish   
   #This is the execution time of mojo in seconds, gotten from mojo functions
    # mojo_result = {'recursion_mojo': 0.056373780000000005, 
    #                'iteration_mojo': 4.7299999999999988e-06,
    #                'iter_optim_mojo': 4.9999999999999998e-08}
    
    func_py = [fb.recursion_py] #, fb.iteration_py, fb.iter_optim_py
    py_result = ft.measure_time_py(fib_num=nth_fib_num, num_iter=iteration_num, func=func_py)
    # combine_result = {**py_result, **mojo_result}

    #Print the execution time of python for each function
    ft.display_result(py_result)

    # #Gather x and y values for plotting
    # plot_values = plots.plotting_value(combine_result)

    # # Plot results
    # plots.plot_graph(plot_values)

main()