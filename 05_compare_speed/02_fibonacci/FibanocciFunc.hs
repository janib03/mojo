module FibanocciFunc (recursion_hs) where


recursion_hs :: Integer -> Integer 
recursion_hs n
    | n <= 1 = n
    | otherwise = recursion_hs(n-1) + recursion_hs(n-2)
