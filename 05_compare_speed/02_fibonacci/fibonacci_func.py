
import numpy as np

def recursion_py(num: int):        
    if num <= 1:
        return num
    fib_num = recursion_py(num - 1) + recursion_py(num - 2)
    return fib_num

def iteration_py(num):
    num_list = [0, 1]
        
    if num == 0 or num == 1:
        return num
        
    for i in range(2, num+1):
        num_list.append(num_list[i-1] + num_list[i-2])
    return num_list[-1]

def iter_optim_py(num):
    if num <= 1:
        return num
    first_num, second_num = 0, 1
    
    for _ in range(2, num + 1):
        first_num, second_num = second_num, first_num + second_num
    return second_num


