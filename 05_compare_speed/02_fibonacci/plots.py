import matplotlib.pyplot as plt
import numpy as np

def plotting_value(merged_time: dict):
    merged_time = dict(sorted(merged_time.items(), key=lambda key_value: key_value[1], reverse=True))
      
    relative_time = {k: max(merged_time.values())/v for k, v in merged_time.items()}
    return relative_time

def plot_diff_methods(plot_values: tuple):
    """" This is for plotting all methods used for calculating Fibanocci number"""
    # Asseble values
    x_value= ["Recursion", "Iteration", "Optimized Iteration"] 
    py_value_y = [plot_values["recursion_py"], plot_values["iteration_py"], plot_values["iter_optim_py"]]
    mojo_value_y = [plot_values["recursion_mojo"], plot_values["iteration_mojo"], plot_values["iter_optim_py"]]
    
    bar_width = 0.35
    pos1 = np.arange(len(x_value))
    pos2 = bar_width + np.arange(len(x_value))
    plt.figure(figsize=(8, 6)) 
    plt.bar(pos1, py_value_y, width=bar_width, label="Python", color="blue", edgecolor="white")
    plt.bar(pos2, mojo_value_y, width=bar_width, label="Mojo", color="orange", edgecolor="white")
    plt.title(f"Relative Performance of Programming Languages")
    plt.ylabel("Execution Speed")
    plt.xlabel("Programming Languages", labelpad=5.5)

    #Add value at top of bar
    for i, j in enumerate(py_value_y):
        plt.text(pos1[i], j + 0.05, f"{round(j,2)}", ha='center', va='bottom')

    for x, y in enumerate(mojo_value_y):
        plt.text(pos2[x], y + 0.05, f"{round(y,2)}", ha='center', va='bottom')

    plt.xticks(pos1 + 0.15, x_value)
    plt.legend()
    plt.show()

def plot_recursion(input_: dict):
    sorted_dict = dict(sorted(input_.items(), key=lambda key_value: key_value[1], reverse=True))
    sorted_dict = {k: max(sorted_dict.values())/v for k, v in sorted_dict.items()}
    x_value = list(sorted_dict.keys())
    y_value = list(sorted_dict.values())
    plt.figure() 
    colors = plt.colormaps['tab10']
    color_range = range(len(x_value))
     
    plt.bar(x_value, y_value, color=colors(color_range),)
    plt.title(f"Relative Performance of Programming Languages")
    plt.ylabel("Execution speed")
    plt.xlabel("Programming Languages")
    #plt.ylim(0,105)

    for index, value in enumerate(y_value):
        #plt.text(index, value+0.5, f"{round(value,3)}%", ha = 'center')
        formatted_value = "{:,.0f}x".format(round(value))
        plt.text(index, value + 0.05, formatted_value, ha='center', va='bottom')
    plt.show()
<<<<<<< HEAD

#x = {"C++": 0.098428, "C": 0.097570, "Mojo": 0.0536247719, "Haskell": 0.14860259, "Python": 38.82756280899048}

#resullts Janina:
x = {"C++": 0.073318, "C": 0.102212, "Mojo": 0.033447550999999999, "Haskell": 0.10387449, "Python": 2.699309730529785}

plot_all_lang(x)
=======
>>>>>>> 295e21cf2917d5c5ec00bcbe508bbe0f9df8f093
