#include <bits/stdc++.h>
using namespace std;
 
int recursion_cpp(int num)
{
    if (num <= 1)
        return num;
    return recursion_cpp(num - 1) + recursion_cpp(num - 2);
}
 
int main() {
    int num = 35; // replace with the desired Fibonacci number
    int result;

    clock_t start_time, end_time;
    double time_diff;

    // Run the recursion_cpp function 10 times and measure the time
    for (int i = 0; i < 10; i++) {
        start_time = clock();
        result = recursion_cpp(num);
        end_time = clock();
        time_diff = ((double) (end_time - start_time)) / CLOCKS_PER_SEC;
        
    }
    cout << num <<"th FIBONACCI NUMBER: " << result << endl;
    cout << "AVERAGE TIME IN SECS: " <<  time_diff << endl;
    return 0;
}