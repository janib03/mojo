#include <stdio.h>
#include <time.h>

int recursion_c(int num)
{
    if (num <= 1)
        return num;
    return recursion_c(num - 1) + recursion_c(num - 2);
}
 
int main()
{
    int num = 35;
    int i, result;
    clock_t start, end;
    double cpu_time_used;
    // printf("%dth Fibonacci Number: %d\n", num, recursion_c(num));

     for (i = 0; i < 10; i++) 
     {
        start = clock();
        result = recursion_c(num);
        end = clock();

        cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
        }
    printf("Fibonacci Number: %d", result);
    printf("\nAVERRAGE TIME IN SECONDS %f: \n", cpu_time_used);
    return 0;
}