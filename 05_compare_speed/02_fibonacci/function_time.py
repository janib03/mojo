import time
import fibonacci_func as fc

def measure_time_py(fib_num: int, num_iter, func: list) -> dict:
    py_time = {}  

    for f in func:
        tim_sum = 0.0
        for _ in range(num_iter):
            start_time = time.time()
            f(fib_num)
            execution_time = time.time() - start_time

            tim_sum += execution_time
        py_time[str(f.__name__)] = (tim_sum/num_iter)
    
    return py_time

def display_result(result: dict) -> None:
    for res in result:
        print(f"AVERAGE {res.upper()} TIME In SECS: {result.get(res, 'Missing Value')}")




