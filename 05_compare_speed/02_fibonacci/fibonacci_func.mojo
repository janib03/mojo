import time
 
fn recursion_mojo(nth_num: Int) -> Int:        
    if nth_num <= 1:
        return nth_num
    
    let fib_num = recursion_mojo(nth_num - 1).__add__(recursion_mojo(nth_num - 2))
    return fib_num

fn iterations_mojo(num: Int) raises -> object:
    let num_list = object([0, 1])
 
    if num == 0 or num == 1:
        return num

    for i in range(2, num+1):
        num_list.append(num_list[i-1].__add__(num_list[i-2]))

    let last_value = num_list.__len__() - 1 
    return num_list.__getitem__(last_value)

fn iter_optim_mojo(num: Int) -> Int:
    if num <= 1:
        return num
    var first_num: Int = 0
    var second_num: Int = 1
    
    for _ in range(2, num + 1):
        first_num, second_num = second_num, first_num + second_num
    return second_num

fn measure_time[num: Int](num_iter: Int, f: fn(nums: Int) -> Int) raises-> object:
    var total_time = SIMD[DType.float64,1](0.0)

    for _ in range(num_iter):
        let start_time = time.now()
        let f_elapse_time = f(num)
        let time_diff = (time.now().__sub__(start_time))/(10**9)
        total_time += time_diff

    let average_time = total_time/num_iter
    return average_time

fn time_iterations_mojo(num_iter: Int, num: Int) raises -> object:
    var total_time = SIMD[DType.float64,1](0.0)

    for j in range(num_iter):
        let start_time = time.now()
        let function = iterations_mojo(num)
        let time_diff = (time.now().__sub__(start_time))/(10**9) 
        total_time += time_diff

    let average_time = total_time/num_iter
    return average_time 

