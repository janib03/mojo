import fibonacci_func as fb

fn main() raises:
    let num_iteration = 10
    
    print_no_newline("AVERAGE RECURSION TIME In SECS: ")
    print(fb.measure_time[35](num_iteration, fb.recursion_mojo)) # 35 has o be passed as concrete value

    # print_no_newline("AVERAGE ITERATION TIME IN SECS: ")
    # print(fb.time_iterations_mojo(num_iteration, 35))

    # print_no_newline("AVERAGE OPTIMIZED ITERATION TIME IN SECS: ")
    # print(fb.measure_time[35](num_iteration, fb.iter_optim_mojo))