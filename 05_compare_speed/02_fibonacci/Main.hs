module Main where
import System.CPUTime
import FibanocciFunc (recursion_hs)


main :: IO()
main = do
    let n = 40
        num_iter = 10

    start_time <- getCPUTime
    let result = map(const $ recursion_hs n) [1..num_iter]
    let val = head result
    print $ val

    end_time <- getCPUTime
    let elapsed_time = fromIntegral (end_time - start_time)/(10^12)
    let avg_time = elapsed_time/num_iter

    putStrLn $ "The" ++ show n ++ "th Fibanocci number is: " ++ show val 
    putStrLn $ "AVERAGE TIME: " ++ show avg_time ++ " SECS"
