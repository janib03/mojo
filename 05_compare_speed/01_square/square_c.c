#include <time.h>
#include <stdio.h>

int main()
{
    int n;
    int rounds = 100;
    int a=0;
    printf("Enter a number: ");
    scanf("%d", &n);
    
    double time_spent[rounds];
    for(int t =0; t<rounds;t++){
        a=0;
        clock_t start = clock();
        for(int i = 0; i <n; i++){
            for(int j = 0; j <n; j++){
                a +=1;
            }
        }
        clock_t end = clock();
        time_spent[t] = ((double) (end - start)) / CLOCKS_PER_SEC;
    }
    
    
    double result_time = 0;
    for(int t =0; t<rounds;t++){
        result_time += time_spent[t];
    }
    result_time /= rounds;
    printf("The square of %d is: %d \n" ,n, a);
    printf("Time to calculate: %f seconds \n", result_time);
    return 0;
}