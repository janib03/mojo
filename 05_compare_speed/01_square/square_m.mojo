from python import Python

fn main()raises:
    let py = Python.import_module("builtins") # to use python 'input'
    let datetime = Python.import_module("datetime")
    let n = py.int(py.input("Enter a number: "))
    
    let now = datetime.datetime.now()
    var time_diff = now - now #get right type for variable 8messy, since it's a Python type

    var a = 0
    for i in range(100):
        a = 0
        let start = datetime.datetime.now()
        # calculate square of input value <n>
        for i in range (n):
            for j in range (n):
                a += 1
        let end = datetime.datetime.now()
        time_diff += (end - start) # accumulator for avarage
        
    time_diff = time_diff/100 #get avarage calculation time
    print("The square of ",n," is: ",a)
    print("Time to calculate: ",time_diff.total_seconds()%60," seconds")

