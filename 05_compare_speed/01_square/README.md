# Square

Main file:
- square.mojo

## Mojo

`square_m.mojo` calculates the square-number of a given numer, only by adding the same number up in a double for-loop. The function prints the result value and the execution time in seconds.
Run either `mojo square_m.mojo` or run the executable with `./square_m` (on Ubuntu)

## Other languages

To compare the speed of Mojo with Python and C, this function is also implemented in `square_p.py` and `square_c.c`.

Run `square_p.py`

To compile `square_c.c`:
> gcc -o square_c square_c.c

Run with:
> ./square_c