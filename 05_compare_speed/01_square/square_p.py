import datetime

n = int(input("Enter a number: "))
a = 0
now = datetime.datetime.now()
time_diff = now - now
for i in range (100):
    a=0
    start = datetime.datetime.now()
    for i in range (n):
        for j in range (n):
            a += 1
    end = datetime.datetime.now()
    time_diff += end - start
time_diff= time_diff/100
print("The square of ",n," is: ",a)
print("Time to calculate: ",time_diff.total_seconds()%60," seconds")