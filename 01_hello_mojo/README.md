# Hello Mojo

This folder contains two basic examples to get familiar with mojo: the helloMojo.mojo and varLet.mojo file. helloMojo is a simple hello world program and varLet gives examples about typing and how to call function in Mojo.

Both files are ready to run via `mojo <file-name>`