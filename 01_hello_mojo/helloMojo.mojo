# To run mojo files a main-function is needed. 
# Only in REPL environment and Jupyter Notebook main-function can be ommited. 
# Otherwise is the start of every Mojo program the main-function.

var m = "mojo!" 

fn main():
    print("hello", m) # sometimes the interpreter (mojo run halloMojo.mojo) does not detect m as variable and prints only "hello"
    print("Bye", m)