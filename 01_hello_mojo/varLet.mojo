# Typing is a thing in mojo fn- functions. In def-functions typing can be omitted. 

fn main()raises:
    var x:Int = 1 # variable type Int
    let y:Int = 1 # constant type Int
    
    print("start: x =",x ,"y =",y)
    add_b2a(x, x) #call function add_b2a
    print("x =",x ,"y =",y) # x+x >> 6 | x is changed by function
    add_b2a(x,y)
    print("x =",x ,"y =",y)
    # add_b2a(y,x) ## not allowed, since y is immutable

    x = 1
    print()
    print("second try: x = ",x ,"y = ",y)
    _ = add2(x, x) #call def-function, returns by default none
    print("x =",x ,"y =",y) 
    _ = add2(x,y)
    print("x =",x ,"y =",y)
    _ = add2(y,x) # allowed, since def chnges only copy of y
    print("x =",x ,"y =",y)


fn add_b2a(inout a:Int, b:Int): #type definition is enforced
    a += b
    # b+=1 ## not allowed, since b is borrowed
    print("a =",a ,"b =",b)

def add2(a,b): # a, b are just recieved copies: changable within the function but changes are not visible outside the function
    # additional remark: print() doesn't work in 'def' as well as in 'fn'
    a += b
    print_no_newline('a = ' )
    print(a)