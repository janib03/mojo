from dataStructs import  Stack #import from own Module dataStructs

fn main():
    var s = Stack() # create Stack
    print("push 10 to s.")
    s.push(10)
    print("push 20 to s.")
    s.push(20)
    print_no_newline("s: ")
    s.print()
    print("copy s to b.")
    var b=s # copy stack s to b
    print("Is b full? ", b.isFull())
    print("pop ", s.pop(), " from s.")
    print("pop ", s.pop(), " from s.")
    print_no_newline("s: ")
    s.print()
    print_no_newline("b: ")
    b.print()
    print("pop ", b.pop(), " from b.")
    print("pop ", b.pop(), " from b.")
    print("pop ", b.pop(), " from b.") # b should be empty -> error