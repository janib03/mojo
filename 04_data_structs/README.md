# Data Structs (Stack)

`dataStructs.mojo` implements the struct `Stack`. This file can't be executed and is only imported as a module into `testDataStructs.mojo`.
`testDataStructs.mojo` can be executed via `mojo testDataStructs.mojo` or calling the executable. It shows simple usage of the afore mentioned stack.

However, all interesting features of mojo are used in the struct `Stack`, so please have a look at the file `dataStructs.mojo` as well.