from memory import memset_zero

struct Stack:
     var data: Pointer[Int] # memory Pointer Type Int
     var top : Int
     var size : Int
        
    fn __init__(inout self, stackSize: Int = 16): # initialize all struct variables stacksize default value = 16
        self.top = -1
        self.size = stackSize
        self.data = Pointer[Int].alloc(self.size)
        memset_zero(self.data, self.size) # fills memory of pointer for self.size elements with 0
        # for i in range(self.size):
        #     self.data.store(i, 0)

    fn __copyinit__(inout self, existing: Self): # make sure we can copy data to another variable 
        self.size = existing.size
        self.top = existing.top
        self.data = Pointer[Int].alloc(self.size)
        for i in range(self.size):
            self.data.store(i, existing.data.load(i))
    
    fn __del__(owned self): # free memory after no use of stack
        self.data.free()
        
    #### Stack functions ####
    fn isFull(self) -> Bool: # no inout needed, since we don't want to change the Stack (self)
        return self.top == self.size
    
    fn push(inout self,value : Int): # inout needed, since we alter the Stack (self)
        if self.isFull():
            print('Stack is full')
            return
        self.top = self.top + 1
        self.data.store(self.top, value)
               
    fn pop(inout self) -> Int : # inout needed, since we alter the Stack (self)
        if self.top <= -1:
            print_no_newline("Stack Error ")
            return -1 # can't return None, since return type eq Int; we could raise an error, but that's to laborious
        let popval = self.data[self.top]
        self.data.store(self.top, 0)
        self.top = self.top - 1
        return popval   

    fn print(self): # no inout needed, since we don't want to change the Stack (self)
        print_no_newline("[")
        for i in range(self.size):
            if i > 0:
                print_no_newline(", ")
            print_no_newline(self.data.load(i))
        print("]")     
