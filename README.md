# Mojo

Kingsley Chimezie Nwokebirinwa, Janina Nicole Berger

## Install Mojo SDK

Mojo SDK is available for Linux and MacOS. In Windows you can run Mojo SDK via WSL (Ubuntu 22.04).
A description provided by Modular to install Mojo SDK, you can find here: (https://developer.modular.com/download) To get acces to the page you need to register with an email. See also [this video part](https://www.youtube.com/watch?v=5Sm9IVMet9c&t=726s) as example for setting up modular/mojo in Windows WSl.

To install Mojo successfully, some Python dependancies are required, which usually can be easily installed via "pip install -module name-".

After installing Mojo you have to set some environment variables:

> Before you start:
> 
> You must set the MODULAR_HOME and PATH environment variables, as described in the output when you ran modular install mojo. For example, if you’re using bash or zsh, add the following lines to your configuration file (.bash_profile, .bashrc, or .zshrc):
> 
> export MODULAR_HOME="\$HOME/.modular" 
>
> export PATH="\$MODULAR_HOME/pkg/packages.modular.com_mojo/bin:\$PATH"
> 
> Then source the file you just updated, for example:
> 
> source ~/.bash_profile

Also, consider installing mojo extension for VS Code to get syntax highlighting.

## Execute and compile mojo-file

Mojo source files are identified with either the .mojo or .🔥 file extension.

### Run Mojo-file
You can instantly execute a mojo file using the `mojo` or `mojo run` command. Navigate to the directory of your file (e.g. `hello.mojo`) and type in your console
> mojo run hello.mojo

### Build Mojo-file
It is also possible to create an executable file with the command `mojo build`. Navigate to the directory of your file (e.g. `hello.mojo`) and type in your console
> mojo build hello.mojo

It creates the executable with the same name as the .mojo file, but you can change that with the -o option. (In this case `hello`.)

To run the executable type:
>./hello

(Disclaimer: substitue "hello" with executable name of your target program)

If Error `Mojo/Python interoperability error: Unable to locate a suitable libpython, please set 'MOJO_PYTHON_LIBRARY'` occurs try run
> echo 'export MOJO_PYTHON_LIBRARY="<full_path_to_libpython.so>"' >> ~/.bashrc
> 
> source ~/.bashrc

See also [troubleshoot MOJO_PYTHON_LIBRARY](https://github.com/modularml/mojo/issues/551)

! Note: all attached executable from mojo files run presumably only on Ubuntu Linux !

# How to navigate our folders

We have 5 folders with different example codes written in (mostly) mojo.

Here is how we recommand looking into those examples:

Best start with folder `01_hello_mojo`. The examples here are very basic and easy to understand, even withot knowledge about mojo.

Then you can look into folder `02_data_analysis`. Here is shown how to import a python file into your mojo code. The python file is not important to understand the mojo part, but feel free to also have a look at it.

`03_pointer` introduces the mojo struct `Pointer`. The simple usage of some of it's key functions are explained/shown here.

`04_data_struct` uses the Pointer struct to build a Stack-struct. The Stack-struct, moreover, shows how to use special methods like \_\_init\_\_ . 

In the last folder `05_compare_speed` we take a look at mojo's performance, by measuring the execution time of two different functions: fibonacci and square. In both sub-packages we implemented the algorithm-in-question in mojo and also in python and c (for fibonacci also in haskel and c++). Further explenation on how to use/run the code there and which files are to be executed, you can find in the readme-file in the corresponding folders.