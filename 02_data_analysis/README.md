# Python Integration

`python_integration.mojo` shows how to import and use self-written python files (in this example get_countries.py).

Execute by running 

> mojo python_integration.mojo
