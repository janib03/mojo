import pandas as pd
import numpy as np
import pycountry

df = pd.read_csv("https://raw.githubusercontent.com/owid/co2-data/master/owid-co2-data.csv")

def country_and_non_country_df(source_df: pd.DataFrame) -> pd.DataFrame:
    
    """
    Aim: to divide the dataframe into countries and non countries. 
    And filter out non co2 columns.
    """
    # 1. Filter columns (country, year, iso_code, co2) using pandas .str.contains()
    strings_of_interest = 'country|year|iso_code|co2'
    boolean_array = source_df.columns.str.contains(strings_of_interest)

    # 2. Array of filtered columns containing country, year, iso_code and all co2 types
    filtered_columns = source_df.columns[boolean_array]

    # 3. Dataframe of filtered columns based on step 2
    df_filtered_column = source_df[filtered_columns]

    # 4. Get list of countries iso code using python pycountry library.
    # In the library the iso_code is repremented with alpha_3.
    list_of_iso_code = [country.alpha_3 for country in pycountry.countries]

    # 5. Filter the dataframe using the list of iso codes. 
    bool_var1 = df_filtered_column['iso_code'].isin(list_of_iso_code)
    df_countries_1 = df_filtered_column.loc[bool_var1] 
   
    # 6. I noticed that Kosovo was missing because it has no iso_code in our original  
    # dataframe, hence, it is excluded during step 5. Next is to get list of countries.
    unique_countries = df_countries_1.country.unique() # array
    list_of_countries = list(unique_countries) # convert array to list
    
    # 7. Add Kosovo to list of countries. Note: the extend method returns non so 
    # it shouldn't be assign to a variable.
    list_of_countries.extend(['Kosovo'])
    
    # 8. Now filter dataframe from step 3 with our new list of countries that includes Kosovo
    bool_var2 = df_filtered_column['country'].isin(list_of_countries)
    df_countries_2 = df_filtered_column.loc[bool_var2] 
    
    # 9. Use the negation sign (~) on bool_var2 to get non countries dataframe
    df_non_countries = df_filtered_column.loc[~bool_var2]
    
    return df_countries_2, df_non_countries

def print_country():
    countries, _ = country_and_non_country_df(df)
    print(countries)

def print_non_country():
    _, non_countries = country_and_non_country_df(df)
    print(non_countries.country.unique())
