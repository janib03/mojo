
from python import Python

# numpy and pandas are required in the imported python file

fn main() raises:
    Python.add_to_path(".")  # local directory (current folder)
    let mypy = Python.import_module("get_countries")
    _ = mypy.print_non_country()
    print()
    _ = mypy.print_country()
    
