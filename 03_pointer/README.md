# Pointer

This folder contains an simple example to the use of the Pointer struct in mojo.
To execute the pointer.mojo file run

> mojo pointer.mojo

or call the executable `./pointer` (if you have Ubuntu)