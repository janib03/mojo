from memory.unsafe import Pointer
from memory import memset_zero

fn main()raises:
    let ptr = Pointer[Int].alloc(4) # create pointer type Int with 4 allocated memory spaces
    memset_zero(ptr, 4) # fills memory of pointer for 4 elements with 0
    
    ptr.store(0, 13) # store value 13 on position 0
    ptr.store(2, 7) # store value 7 on position 2

    # print all values in Pointer space:
    print_no_newline("[")
        for i in range(4):
            if i > 0:
                print_no_newline(", ")
            print_no_newline(ptr.load(i)) # 'load' aka retrieve value from position i 
        print("]")   

    print("ptr[0]: ", ptr[0])
    print("ptr.load(0): ", ptr.load(0))
    
    # ptr.store(-1, 13)
    print("ptr[-1]: ", ptr[-1]) #read space ``outside'' pointer-memory
    print("ptr[200]: ", ptr[200]) #read space ``outside'' pointer-memory

    # ptr.store(-2,13)
    ptr.free() 
